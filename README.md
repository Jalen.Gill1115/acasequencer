AcaSequencer!

This project is powered by Reactronica, which is a library that allows you to sequence and compose music.

Eventually, I plan to use custom vocal sounds as the instrument, but am using the default instrument for now.
